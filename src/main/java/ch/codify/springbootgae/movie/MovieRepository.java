package ch.codify.springbootgae.movie;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MovieRepository extends CrudRepository<Movie,Long> {
    List<Movie> findAll();

    Optional<Movie> findByName(String name);

    Movie save(Movie movie);

    void delete(Movie movie);
}
