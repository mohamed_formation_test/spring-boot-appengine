package ch.codify.springbootgae;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@SpringBootApplication
public class SpringBootGaeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGaeApplication.class, args);
	}

}
